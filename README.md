# Criptografía - Bootcamp Cybersecurity III

Proyecto académico con el objetivo de aplicar técnicas y herramientas aprendidas durante el curso ([enunciado](./Enunciado_Practica_Criptografia.pdf)).

## Herramientas

Se han utilizado los siguientes sistemas operativos y herramientas en la realización de los ejemplos:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.12 r139181 (Qt5.6.2).
* [Kali Linux](https://www.kali.org/get-kali/#kali-virtual-machines) 2021.4-virtualbox-amd64.
  * [Git](https://www.git-scm.com/) 2.32.0.
  * [Python](https://www.python.org/downloads/) 3.9.2.
  * [Peek](https://github.com/phw/peek) 1.5.1.

## Contenido

* [Ejercicio 1](./01/README_EJERCICIO_01.md).
* [Ejercicio 2](./02/README_EJERCICIO_02.md).
* [Ejercicio 3](./03/README_EJERCICIO_03.md).
* [Ejercicio 4](./04/README_EJERCICIO_04.md).
* [Ejercicio 5](./05/README_EJERCICIO_05.md).
* [Ejercicio 6](./06/README_EJERCICIO_06.md).
* [Ejercicio 7](./07/README_EJERCICIO_07.md).
* [Ejercicio 8](./08/README_EJERCICIO_08.md).
* [Ejercicio 9](./09/README_EJERCICIO_09.md).

## Corrección del profesor

Considero tu práctica APTA (más detalles en cada ejercicio).
