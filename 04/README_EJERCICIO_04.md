# Criptografía - Ejercicio 4

## Enunciado

Existe otro error en la implementación del Server (del [ejercicio 2](../02/README_EJERCICIO_02.md)) que le puede hacer vulnerable a otro tipo de ataques, concretamente en los parámetros que usa para CTR.

* ¿Sabrías decir cuál es?
* ¿Cómo lo solucionarías?

## Solución

Información:

* Valores que recibe la función de cifrado de la *cookie* en el algoritmo AES CRT:
  * La *cookie* a cifrar.
  * El *nounce*.
  * La *key*.

Problemas detectados:

* Se está generando una única vez el valor del parámetro *nonce* y se está reutilizando el mismo valor para cifrar todas las *cookies*.
* Reutilizar el mismo valor *nonce* para generar el *keystream* hace vulnerable el algoritmo de cifrado.

Pasos utilizados para solucionar la vulnerabilidad:

* Se ha generado el valor del parámetro *nonce* en cada petición de obtención de la *cookie*.
* El método que genera la *cookie* devuelve al cliente:
  * El *nonce* utilizado en el cifrado.
  * La cookie cifrada.
* En las peticiones de validación de la *cookie* se recibe:
  * La *cookie* cifrada.
  * El valor del parámetro *nonce* que se utilizó en el cifrado.

Para evitar esta vulnerabilidad se ha modificado el código del servidor en el script [nonce.py](nonce.py).

## Corrección del profesor

Has identificado correctamente el problema. Como detalle de implementación, es bastante común, en vez de pasar el nonce y el ciphertext como dos strings separadas, poner el nonce al principio de la string y el ciphertext después, en una sola string. Y el método de decrypt primero separaría el nonce sabiendo el tamaño.
