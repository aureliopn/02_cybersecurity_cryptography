# Criptografía - Ejercicio 1

## Enunciado

Junto a este documento se adjunta un archivo [ej1.txt](./ej1.txt) que contiene *ciphertext* en base64.
Sabemos que el método para encriptarlo ha sido el siguiente:

La clave es de 10 caracteres. Frodo ha pensado que era poco práctico implementar *one time pad*, ya que requiere claves muy largas (tan largas como el mensaje). Por lo que ha decidido usar una clave corta, y repetirla indefinidamente. Es decir el primer byte de *cipher* es el primer byte del *plaintext* XOR el primer byte de la clave. Como la clave es de 10 caracteres, sabemos que el carácter 11 del ciphertext también se le ha hecho XOR con el mismo primer carácter de la clave. Debajo vemos un ejemplo donde la clave usada es “ABC”.

![XOR_ABC](./img/01-xor-abc.jpg)

¿Por qué no ha sido una buena idea la que ha tenido Frodo? Explica cómo se puede encontrar la clave y desencriptar el texto a partir del archivo.
Programa el código y encuentra la clave junto con el texto.
Pista: no debería ser muy costoso, ya que se puede reutilizar mucho código de un ejercicio hecho en clase ;)

## Solución

Información:

* *Ciphertext* en base64.
* Cifrado XOR.
* Clave de 10 caracteres con repetición.

No es buena idea repetir la clave para cifrar el mensaje porque:

* El cifrado es vulnerable a un ataque de fuerza bruta sobre cada bloque resultante de dividir el mensaje cifrado entre la longitud de la clave.
* Cuanto mayor es la longitud del mensaje, y menor es la longitud de la clave, el ataque de fuerza bruta se realiza sobre más bloques, y la probabilidad de descubrir la clave aumenta.

Pasos utilizados para descifrar el mensaje:

* Convertir el *Ciphertext* de base64 a bytes.
* Dividir el bloque de bytes del *Ciphertext* en bloques de la misma longitud que la clave.
* Si el último de los bloques en los que se ha dividido el *Ciphertext* tiene menos longitud que la clave, rellenar el final del bloque para que todos los bloques tengan la misma longitud.
* Ejecutar ataque de fuerza bruta para cada byte de la clave:
  * Probando todos los valores posibles entre 0 y 128 (valores ASCII).
  * Eligiendo como byte candidato para la clave aquel que haya obtenido mejor puntuación relativa a su frecuencia en el alfabeto inglés.

Ejecutando con Python el script [decrypt.py](./decrypt.py) se obtiene:

```text
└─$ python3 decrypt.py
Candidate key:
b'keepcoding'
Decrypted message:
b"We're no strangers to love\nYou know the rules and so do I\nA full commitment's what I'm thinking of\nYou wouldn't get this from any other guy\nI just wanna tell you how I'm feeling\nGotta make you understand\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry\nNever gonna say goodbye\nNever gonna tell a lie and hurt you\nWe've known each other for so long\nYour heart's been aching but you're too shy to say it\nInside we both know what's been going on\nWe know the game and we're gonna play it\nAnd if you ask me how I'm feeling\nDon't tell me you're too blind to see\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry\nNever gonna say goodbye\nNever gonna tell a lie and hurt you\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry\nNever gonna say goodbye\nNever gonna tell a lie and hurt you\nNever gonna give, never gonna give\n(Give you up)\n(Ooh) Never gonna give, never gonna give\n(Give you up)\nWe've known each other for so long\nYour heart's been aching but you're too shy to say it\nInside we both know what's been going on\nWe know the game and we're gonna play it\nI just wanna tell you how I'm feeling\nGotta make you understand\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry\nNever gonna say goodbye\nNever gonna tell a lie and hurt you\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry\nNever gonna say goodbye\nNever gonna tell a lie and hurt you\nNever gonna give you up\nNever gonna let you down\nNever gonna run around and desert you\nNever gonna make you cry"
```

![Decrypt](./img/02-decrypt.jpg)

Con la clave "keepcoding" se descifra el mensaje:

```text
We're no strangers to love
You know the rules and so do I
A full commitment's what I'm thinking of
You wouldn't get this from any other guy
I just wanna tell you how I'm feeling
Gotta make you understand
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
We've known each other for so long
Your heart's been aching but you're too shy to say it
Inside we both know what's been going on
We know the game and we're gonna play it
And if you ask me how I'm feeling
Don't tell me you're too blind to see
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give, never gonna give
(Give you up)
(Ooh) Never gonna give, never gonna give
(Give you up)
We've known each other for so long
Your heart's been aching but you're too shy to say it
Inside we both know what's been going on
We know the game and we're gonna play it
I just wanna tell you how I'm feeling
Gotta make you understand
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
Never gonna say goodbye
Never gonna tell a lie and hurt you
Never gonna give you up
Never gonna let you down
Never gonna run around and desert you
Never gonna make you cry
```

## Corrección del profesor

Has encontrado la solución correcta, y el método es el esperado, muy bien. En cuanto a añadir bytes al último bloque para que sean todos del mismo tamaño, realmente no es necesario, y depende de lo que pongas podría confundir al análisis de frecuencia.
