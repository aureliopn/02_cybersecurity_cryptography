# Criptografía - Ejercicio 2

## Enunciado

En el archivo [ej2.py](./ej2.py) encontramos dos funciones. Una de ellas obtiene una string cualquiera y genera una *cookie* encriptada con AES CTR, la función genera cuentas con rol user, y se encarga de verificar que la string no contiene ni ‘;’ ni ‘=’.

La otra función desencripta la *cookie* y busca la string ‘;admin=true;’. Realizando llamadas a estas dos funciones, genera una *cookie* y modifica el *ciphertext* tal que al llamar la función de check te devuelva permisos de admin. (Obviamente se puede acceder a la clave de encriptación de la clase, pero el ejercicio consiste en modificar el sistema tal y como si el código se ejecutara en un servidor remoto al que no tenemos acceso, y solo tuvieramos acceso al código).

Pista: Piensa en qué efecto tiene sobre el *plaintext* en CTR cuando cambia un bit del *ciphertext*.

## Solución

Información:

* Función de generación de la *cookie*:
  * Verificación de no existencia de caracteres ‘;’ ni ‘=’.
  * Cifrado AES CTR.
* Función de validación de la *cookie*:
  * La *cookie* debe estar correctamente cifrada.
  * Si la información de la *cookie* contiene el texto ‘;admin=true;’ otorga privilegios de administrador.

Pasos utilizados para obtener privilegios de administrador llamando a la función de validación de la *cookie*:

* Obtener el *ciphertext* de la *cookie* enviando "ure|admin_true" (sabemos que su *plaintext* será "cookieversion=2.0;userdata=ure|admin_true;safety=veryhigh").
* Sustituir el byte *A* correspondiente al índice 30 del *ciphertext* (que representa el caracter "|" que queremos cambiar al caracter ";"), por el *A'* obtenido de la siguiente manera:
  * A XOR ord('_') = Key
  * Key XOR ord('=') = A'
* Sustituir el byte *B* correspondiente al índice 36 del *ciphertext* (que representa el caracter "_" que queremos cambiar al caracter "="), por el *B'* obtenido de la siguiente manera:
  * B XOR ord('_') = Key
  * Key XOR ord('=') = B'
* Enviar la *cookie* modificada.

Se ha realizado una copia del script original para realizar el ejercicio. Ejecutando con Python el script [hack.py](./hack.py) se obtiene:

```text
└─$ python3 hack.py
Acceso Admin!
```

![AES CTR HACK](./img/01-aes-ctr-hack.jpg)

Se demuestra que el *ciphertext* de la *cookie* se puede alterar maliciosamente conociendo su *plaintext*.

## Corrección del profesor

Tu solución es correcta. En CTR podemos encontrar el byte de keystream conociendo el plaintext. En el caso que no conozcamos la posición de nuestro input en el plaintext, bastaría con probar con todas las posiciones posibles. Realmente lo que estamos haciendo es bit-flipping, modificando en el ciphertext los bits que tienen que cambiar para cambiar los caracteres deseados.
