from secrets import token_bytes, randbits
from base64 import b64encode
from Crypto.Hash import SHA256
from time import time
import jwt

class AuthServer:
    def __init__(self):
        # inicializar datos del servidor (secreto para JWT)
        self._secret = b64encode(token_bytes(32)).decode() # generar secreto
        # inicializar usuarios (diccionario vacío)
        self._users = {}
        # inicializar intentos de login (diccionario vacío)
        self._login_attempts = {}

    def register(self, user, password):
        # Guarda los datos necesarios para verificar un login en self._users
        if len(password) < 11:
            raise Exception("Password invalid: must have 11 characters")
        h = SHA256.new()
        h.update(bytes(password, 'ASCII'))
        pw_hash = h.hexdigest()
        user_store = (pw_hash)
        self._users[user] = (user_store)
        self._login_attempts[user] = 0
        return

    def login(self, user, password):
        # Verifica que el usuario no está bloqueado por superar el número de intentos seguidos fallidos de login
        if self._login_attempts[user] > 5:
            raise Exception("User blocked: too many login attempts failed")
        # Verifica que el password es correcto para el usuario a partir de los datos guardados en register.
        (stored_pw) = self._users[user]
        h = SHA256.new()
        h.update(bytes(password, 'ASCII'))
        derived_pw = h.hexdigest()
        # Si el password es correcto, devuelve un jwt con el usuario (sub) y la expiracion (exp) en el payload
        if derived_pw == stored_pw:
            self._login_attempts[user] = 0
            expiration = int(time()) + (60*60*6)
            return jwt.encode({'sub': user, 'exp': expiration}, self._secret, algorithm='HS256')
        else:
            self._login_attempts[user] = self._login_attempts[user] + 1
            raise Exception("Bad login attempt " + str(self._login_attempts[user]))

    def verify(self, token):
        # Verifica el JWT, y si es valido devuelve el usuario 'sub' del payload
        payload = jwt.decode(token, self._secret, algorithms=['HS256'])
        if int(time()) > payload['exp']:
            raise Exception("JWT expired.")
        return payload['sub']

def client():
    authServer = AuthServer()

    # Realizamos el registro con una password corta
    print('> Registro con password corta')
    try:
        authServer.register("user", "password")
        print('Registered User.' + ' ❌')
    except Exception as e:
        print(e, '✅')

    passwordOk = "PassworUnP0c0MasLarga!ParaVariar:)"
    authServer.register("user", passwordOk)

    # Realizamos el proceso de login correctamente
    print('> Login con credenciales correctas')
    try:
        token = authServer.login("user", passwordOk)
        authenticatedUser = authServer.verify(token)
        if authenticatedUser == "user":
            print('Authenticated user: ' + authenticatedUser + ' ✅')
        else:
            print('❌')
    except Exception as e:
        print(e, '❌')

    # Realizamos un login con credenciales incorrectas
    print('> Login con credenciales incorrectas')
    try:
        token = authServer.login("user", "1234")
        print('❌')
    except Exception as e:
        print(e, '✅')

    # Enviamos un jwt modificado
    print('> JWT modificado')
    try:
        token = authServer.login("user", passwordOk)
        authenticatedUserOk = authServer.verify(token)
        print('Authenticated user OK: ' + authenticatedUserOk + ' ✅')
        authenticatedUser = authServer.verify(b'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTk4NDU0NjEzfQ.pvFFedY2ByyhXAR_pAXkg6FCmzo81e__fpGB-W77k5M')
        print('Authenticated user: ' + authenticatedUser + ' ❌')
    except Exception as e:
        print(e, '✅')

client()
