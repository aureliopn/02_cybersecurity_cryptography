# Criptografía - Ejercicio 6

## Enunciado

Modifica el código para solucionar las vulnerabilidades encontradas (en el [ejercicio 5](../05/README_EJERCICIO_05.md)).

## Solución

Medidas implementadas para solucionar las vulnerabilidades:

* Aumentar la longitud de la clave privada del servidor a 256 bits.

```python
self._secret = b64encode(token_bytes(32)).decode() # generar secreto
```

* Validar la longitud de la password del usuario con un mínimo de 11 caracteres.

```python
if len(password) < 11:
  raise Exception("Password invalid: must have 11 characters")
```

* Utilizar el algoritmo SHA256 para generar la *hash* con la password del usuario.

```python
from Crypto.Hash import SHA256
...
h = SHA256.new()
...
h = SHA256.new()
```

* Limitar el número de intentos de login seguidos fallidos de cada usuario.

```python
self._login_attempts = {}
...
if self._login_attempts[user] > 5:
  raise Exception("User blocked: too many login attempts failed")
...
if derived_pw == stored_pw:
  self._login_attempts[user] = 0
  ...
else:
  self._login_attempts[user] = self._login_attempts[user] + 1
  ...
```

* Validar la caducidad del JWT.

```python
if int(time()) > payload['exp']:
  raise Exception("JWT expired.")
```

* Eliminar la especificación del algoritmo "none" en la verificación del JWT.

```python
jwt.decode(token, self._secret, algorithms=['HS256'])
```

Se ha realizado una copia del script original para realizar el ejercicio. Ejecutando con Python el script [fix.py](./fix.py) con las correcciones se obtiene:

![Fix AuthServer](./img/01-fix-authserver.jpg)

## Corrección del profesor (para ejercicios 5 y 6)

Has identificado los problemas correctamente, con algún detalle a mejorar. Primero, no es cierto que no se validara la expiración del jwt, ya que la librería de jwt al hacer el decode automáticamente valida la expiración. El problema que había era que la expiración era muy larga. El otro problema más grave que encuentro en tu solución es que cambias sha1 por sha256, que es un hash rápido, que continua sin contener un salt, y por lo tanto no es el más adecuado para contraseñas, sería vulnerable a ataques de rainbow tables igual que sha1.
