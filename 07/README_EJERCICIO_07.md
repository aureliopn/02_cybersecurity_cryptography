# Criptografía - Ejercicio 7

## Enunciado

* ¿Qué problema hay con la implementación de AuthServer (en el [ejercicio 5](../05/README_EJERCICIO_05.md)) si el cliente se conecta a él usando http?
* ¿Cómo lo solucionarías?

## Solución

Información:

* Con el protocolo HTTP el tráfico se transmite sin cifrar.
* Si un atacante intercepta el tráfico entre el cliente y el servidor tendría acceso a toda la información transmitida, incluidas las credenciales de los usuarios.

Para evitar este tipo de ataques se debe utilizar el protocolo HTTPS, en el que toda la información viaja cifrada. Para ello habriá que:

* Solicitar un certificado a una entidad certificadora (CA).
* Utilizar la clave privada del certificado en el *backend* del servicio web para cifrar el tráfico.
* Exponer el certificado en nuestro sitio web.

## Corrección del profesor

Correcto, http es inseguro para un servidor de autenticación, y se debería usar siempre HTTPS.
