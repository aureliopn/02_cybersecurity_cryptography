# Criptografía - Ejercicio 8

## Enunciado

Sabemos que un hacker ha entrado en el ordenador de Gandalf en el pasado. Gandalf entra en internet, a la página de Facebook. En la barra del navegador puede ver que tiene un certificado SSL válido.

* ¿Corre algún riesgo si sigue navegando?

## Solución

Información:

* El ordenador de Gandalf está comprometido.

Una vez que un ordenador está comprometido su uso no es fiable aunque el navegador muestre información válida del certificado de un sitio web. Por ejemplo, es posible que Gandalf sea vícitma de un ataque *phishing* si el atacante ha añadido:

* Un a certificado suyo en las "Trusted Root Certification Authorities".
* Una entrada en el archivo *hosts* para que el dominio a suplantar apunte a la IP del sitio web malicioso que utiliza dicho certificado.

## Corrección del profesor

Respuesta correcta. La seguridad de https depende de los CAs guardados en el sistema. Y si el sistema ha sido comprometido, entonces la seguridad de SSL también.
