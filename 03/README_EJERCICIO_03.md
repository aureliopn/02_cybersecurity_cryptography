# Criptografía - Ejercicio 3

## Enunciado

Explica cómo modificarías el código del [ejercicio 2](../02/README_EJERCICIO_02.md) para protegerte de ataques en los que se modifica el ciphertext para conseguir una *cookie* ‘admin=true’.

## Solución

Información:

* El servidor es vulnerable a ataques en los que se modifica el contenido de la *cookie*.

Para evitar esta vulnerabilidad se podría:

* Generar una clave privada en la inicialización del servidor.
* Firmar el contenido de la cookie con la clave privada del servidor en la generación de la *cookie*.
* Comprobar que la *cookie* está correctamente firmada utilizando la clave privada en la verificación de la *cookie*.

Hay una implementación que evita esta vulnerabilidad utilizando JWT en el [ejercicio 5](../05/README_EJERCICIO_05.md).

## Corrección del profesor

Has identificado correctamente que se trata de un problema de autenticación, quizá en este caso ya que se ha decidido implementar la cookie en formato string y no JSON, JWT no sería la mejor solución de autenticación, y sería más apropiado usar autenticación más generica como una HMAC, o incluso mejor, usar GCM en lugar de CTR.
