# Criptografía - Ejercicio 9

## Enunciado

En el archivo [sergi-pub.asc](./sergi-pub.asc) está mi clave pública PGP:

* Comprueba que la *fingerprint* de mi clave es:

```text
DAB01687C2910408227DD51306D51234C8B631A2
```

* Genera un par de claves, y añade tu clave pública a los archivos entregados, el archivo con tu clave pública tiene que estar encriptado para que lo pueda ver sólo con mi clave privada.
* Usa tu clave privada para firmar el archivo de entrega, y adjunta la firma en un documento firma.sig (Recuerda que deberás hacer la firma con la versión final que entregues, ya que si realizas algún cambio, la firma no será válida).

Explica los pasos que has seguido.

## Solución

Información:

* Clave pública PGP de "Sergi Canal <sergi.cg8@gmail.com>".

### Comprobación del *fingerprint* de la clave pública PGP de Sergi Canal

Con la siguiente instrucción se obtiene el *fingerprint* de la clave pública de Sergi Canal:

```bash
gpg --show-keys --with-fingerprint sergi-pub.asc
```

![Fingerprint Check](./img/01-fingerprint-check.jpg)

El *fingerprint* "DAB01687C2910408227DD51306D51234C8B631A2" coincide. Se ha comprobado que se tiene el fichero correcto con la clave pública PGP de Sergi Canal.

### Generación del par de claves PGP y encriptación de la clave pública generada

Con la siguiente instrucción se genera el par de claves pública y privada PGP:

```bash
gpg2 --full-generate-key
```

![PGP Key Pair Generation](./img/02-pgp-key-pair-generation.gif)

Con la siguiente instrucción se exporta la clave privada PGP generada:

```bash
gpg --export-secret-key -a 36D71031FA0A8A1E8860CEC6015FF91906980C69 > ure-priv.key
```

![PGP Private Key Export](./img/03-pgp-private-key-export.gif)

Con la siguiente instrucción se exporta la clave pública PGP generada:

```bash
gpg --export -a 36D71031FA0A8A1E8860CEC6015FF91906980C69 > ure-pub.key
```

![PGP Public Key Export](./img/04-pgp-public-key-export.jpg)

Con la siguiente instrucción se importa la clave pública de Sergi Canal (necesario para poder encriptar con su clave):

```bash
gpg --import sergi-pub.asc
```

![PGP Public Key Import](./img/05-pgp-public-key-import.jpg)

Con la siguiente instrucción se encripta la clave pública PGP (generada en el ejercicio) con la clave pública de Sergi Canal:

```bash
gpg --recipient sergi.cg8@gmail.com --output ure-pub-encrypted.gpg --encrypt ure-pub.key
```

![PGP Public Key Encryption](./img/06-pgp-public-key-encryption.jpg)

La clave pública PGP (generada en el ejercicio) encriptada con la clave pública de Sergi Canal está en el fichero [ure-pub-encrypted.gpg](./ure-pub-encrypted.gpg).

Sólo quien posea la clave privada de Sergi Canal podrá descrifrar el fichero ejecutando:

```bash
gpg --output ure-pub.key --decrypt ure-pub-encrypted.gpg
```

### Firma del documento de entrega

Se ha generado el [documento.pdf](./documento.pdf) para la realización del ejercicio. Con la siguiente instrucción se firma el documento con la clave privada PGP generada:

```bash
gpg --output firma.sig --detach-sign documento.pdf
```

![PGP Document Signature](./img/07-pgp-document-signature.gif)

La firma del documento está en el archivo [firma.sig](./firma.sig). Sólo quien posea la clave pública PGP generada podrá verificar la firma del documento ejecutando:

```bash
gpg --verify firma.sig documento.pdf
```

![PGP Document Verification](./img/08-pgp-document-verification.jpg)
