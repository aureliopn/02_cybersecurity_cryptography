# Criptografía - Ejercicio 5

## Enunciado

En el archivo [ej5.py](./ej5.py) encontrarás una implementación de un sistema de autenticación de un servidor usando JWT. El objetivo del ejercicio es encontrar posibles vulnerabilidades en la implementación del servidor, y explicar brevemente cómo podrían ser atacadas.

* La función *register* crea un nuevo usuario en el sistema (en este caso guardado localmente en el objeto AuthServer en la lista users).
* La función *login* verifica que el password es correcto para el usuario, y si es así, devuelve un JWT donde sub = user y con expiración 6h después de la creación del token.
* La función verify verifica que la firma es correcta y devuelve el usuario que está autenticado por el token.

Para JWT usamos la librería PyJWT: [https://pyjwt.readthedocs.io/en/latest/](https://pyjwt.readthedocs.io/en/latest/)

Para funciones criptográficas usamos la librería "PyCryptoDome" utilizada anteriormente en clase.

## Solución

Información:

* En la inicialización del servidor se genera una clave privada con un token aleatorio de 4 bytes para cifrar los [JWT](https://jwt.io/).
* En la función *register* no se valida la longitud de la password del usuario.
* En la función *register* se genera una *hash* SHA1 de la password del usuario.
* No hay control sobre el número de intentos de login de cada usuario.
* En la función *verify* no se comprueba que el tiempo de la validez del token haya expirado.
* En la función *verify* se utiliza (aunque no repercute actualmente en una vulnerabilidad):

```python
jwt.decode(token, self._secret, algorithms=['HS256', 'None']) # mejor utilizar: jwt.decode(token, self._secret, algorithms=['HS256'])
```

Vulnerabilidades:

* La clave privada del servidor para firmar los JWT es muy corta.
* Las passwords de pequeña longitud posibilitan el uso de contraseñas débiles, más fáciles de averiguar.
* Sin un control del número de intentos fallidos de cada usuario se posibilitan ataques de fuerza bruta.
* Los JWTs generados, a efectos, no caducan nunca.

Posibles ataques:

* Averiguar la clave privada utilizada para firmar los JWT con fuerza bruta y modificar la información de los JWT que se envían al servidor maliciosamente.
* Revertir las *hashes* de las passwords de los usuarios con fuerza bruta si se posee acceso a la base de datos y:
  * Las passwords son cortas (débiles).
  * El algoritmo de generación de la *hash* es poco seguro (SHA1 es menos seguro que SHA256).
* Atacar con fuerza bruta para averiguar la password de un usuario del que se conoce su nombre.

## Corrección del profesor (para ejercicios 5 y 6)

Has identificado los problemas correctamente, con algún detalle a mejorar. Primero, no es cierto que no se validara la expiración del jwt, ya que la librería de jwt al hacer el decode automáticamente valida la expiración. El problema que había era que la expiración era muy larga. El otro problema más grave que encuentro en tu solución es que cambias sha1 por sha256, que es un hash rápido, que continua sin contener un salt, y por lo tanto no es el más adecuado para contraseñas, sería vulnerable a ataques de rainbow tables igual que sha1.
